<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRecepOxxo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReciboLabel As System.Windows.Forms.Label
        Dim CLIENTELabel As System.Windows.Forms.Label
        Dim Clv_SessionLabel As System.Windows.Forms.Label
        Dim StatusLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.CONSULTA_Resultado_OxxoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.ReciboTextBox = New System.Windows.Forms.TextBox()
        Me.CLIENTETextBox = New System.Windows.Forms.TextBox()
        Me.CONSULTA_Resultado_OxxoTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.CONSULTA_Resultado_OxxoTableAdapter()
        Me.CONSULTATablaClv_Session_OxxoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CONSULTATablaClv_Session_OxxoTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.CONSULTATablaClv_Session_OxxoTableAdapter()
        Me.Clv_SessionTextBox = New System.Windows.Forms.TextBox()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.dgvCargaArchivos = New System.Windows.Forms.DataGridView()
        Me.dgvDetalleArchivoCargado = New System.Windows.Forms.DataGridView()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter3 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter4 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter5 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.Sesion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Consecutivo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sucursal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Hora = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Temporal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDFactura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Factura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Contrato = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        ReciboLabel = New System.Windows.Forms.Label()
        CLIENTELabel = New System.Windows.Forms.Label()
        Clv_SessionLabel = New System.Windows.Forms.Label()
        StatusLabel = New System.Windows.Forms.Label()
        CType(Me.CONSULTA_Resultado_OxxoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CONSULTATablaClv_Session_OxxoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCargaArchivos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDetalleArchivoCargado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReciboLabel
        '
        ReciboLabel.AutoSize = True
        ReciboLabel.Location = New System.Drawing.Point(371, 339)
        ReciboLabel.Name = "ReciboLabel"
        ReciboLabel.Size = New System.Drawing.Size(44, 13)
        ReciboLabel.TabIndex = 6
        ReciboLabel.Text = "Recibo:"
        '
        'CLIENTELabel
        '
        CLIENTELabel.AutoSize = True
        CLIENTELabel.Location = New System.Drawing.Point(360, 365)
        CLIENTELabel.Name = "CLIENTELabel"
        CLIENTELabel.Size = New System.Drawing.Size(55, 13)
        CLIENTELabel.TabIndex = 7
        CLIENTELabel.Text = "CLIENTE:"
        AddHandler CLIENTELabel.Click, AddressOf Me.CLIENTELabel_Click
        '
        'Clv_SessionLabel
        '
        Clv_SessionLabel.AutoSize = True
        Clv_SessionLabel.Location = New System.Drawing.Point(92, 183)
        Clv_SessionLabel.Name = "Clv_SessionLabel"
        Clv_SessionLabel.Size = New System.Drawing.Size(65, 13)
        Clv_SessionLabel.TabIndex = 9
        Clv_SessionLabel.Text = "Clv Session:"
        AddHandler Clv_SessionLabel.Click, AddressOf Me.Clv_SessionLabel_Click
        '
        'StatusLabel
        '
        StatusLabel.AutoSize = True
        StatusLabel.Location = New System.Drawing.Point(377, 148)
        StatusLabel.Name = "StatusLabel"
        StatusLabel.Size = New System.Drawing.Size(40, 13)
        StatusLabel.TabIndex = 11
        StatusLabel.Text = "Status:"
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(814, 11)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(182, 33)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Cargar Archivo"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'CONSULTA_Resultado_OxxoBindingSource
        '
        Me.CONSULTA_Resultado_OxxoBindingSource.DataMember = "CONSULTA_Resultado_Oxxo"
        Me.CONSULTA_Resultado_OxxoBindingSource.DataSource = Me.DataSetEdgar
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(814, 488)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(182, 32)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "&SALIR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(814, 50)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(182, 33)
        Me.Button3.TabIndex = 6
        Me.Button3.Text = "Afectar Clientes"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'ReciboTextBox
        '
        Me.ReciboTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_Resultado_OxxoBindingSource, "Recibo", True))
        Me.ReciboTextBox.Location = New System.Drawing.Point(421, 336)
        Me.ReciboTextBox.Name = "ReciboTextBox"
        Me.ReciboTextBox.Size = New System.Drawing.Size(100, 20)
        Me.ReciboTextBox.TabIndex = 7
        '
        'CLIENTETextBox
        '
        Me.CLIENTETextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTA_Resultado_OxxoBindingSource, "CLIENTE", True))
        Me.CLIENTETextBox.Location = New System.Drawing.Point(421, 362)
        Me.CLIENTETextBox.Name = "CLIENTETextBox"
        Me.CLIENTETextBox.Size = New System.Drawing.Size(100, 20)
        Me.CLIENTETextBox.TabIndex = 8
        '
        'CONSULTA_Resultado_OxxoTableAdapter
        '
        Me.CONSULTA_Resultado_OxxoTableAdapter.ClearBeforeFill = True
        '
        'CONSULTATablaClv_Session_OxxoBindingSource
        '
        Me.CONSULTATablaClv_Session_OxxoBindingSource.DataMember = "CONSULTATablaClv_Session_Oxxo"
        Me.CONSULTATablaClv_Session_OxxoBindingSource.DataSource = Me.DataSetEdgar
        '
        'CONSULTATablaClv_Session_OxxoTableAdapter
        '
        Me.CONSULTATablaClv_Session_OxxoTableAdapter.ClearBeforeFill = True
        '
        'Clv_SessionTextBox
        '
        Me.Clv_SessionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTATablaClv_Session_OxxoBindingSource, "Clv_Session", True))
        Me.Clv_SessionTextBox.Location = New System.Drawing.Point(163, 180)
        Me.Clv_SessionTextBox.Name = "Clv_SessionTextBox"
        Me.Clv_SessionTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_SessionTextBox.TabIndex = 10
        '
        'StatusTextBox
        '
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONSULTATablaClv_Session_OxxoBindingSource, "Status", True))
        Me.StatusTextBox.Location = New System.Drawing.Point(352, 109)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.Size = New System.Drawing.Size(100, 20)
        Me.StatusTextBox.TabIndex = 12
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(814, 89)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(182, 33)
        Me.Button5.TabIndex = 14
        Me.Button5.Text = "Imprimir Facturas"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(814, 128)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(182, 33)
        Me.Button6.TabIndex = 15
        Me.Button6.Text = "Imprimir Detalle"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'dgvCargaArchivos
        '
        Me.dgvCargaArchivos.AllowUserToAddRows = False
        Me.dgvCargaArchivos.AllowUserToDeleteRows = False
        Me.dgvCargaArchivos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCargaArchivos.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvCargaArchivos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvCargaArchivos.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvCargaArchivos.Location = New System.Drawing.Point(12, 13)
        Me.dgvCargaArchivos.Margin = New System.Windows.Forms.Padding(2)
        Me.dgvCargaArchivos.Name = "dgvCargaArchivos"
        Me.dgvCargaArchivos.ReadOnly = True
        Me.dgvCargaArchivos.RowHeadersVisible = False
        Me.dgvCargaArchivos.RowTemplate.Height = 24
        Me.dgvCargaArchivos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCargaArchivos.Size = New System.Drawing.Size(783, 223)
        Me.dgvCargaArchivos.TabIndex = 16
        '
        'dgvDetalleArchivoCargado
        '
        Me.dgvDetalleArchivoCargado.AllowUserToAddRows = False
        Me.dgvDetalleArchivoCargado.AllowUserToDeleteRows = False
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDetalleArchivoCargado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvDetalleArchivoCargado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetalleArchivoCargado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Sesion, Me.Consecutivo, Me.Sucursal, Me.Fecha, Me.Hora, Me.Temporal, Me.Monto, Me.Estado, Me.IDFactura, Me.Factura, Me.Contrato, Me.Nombre})
        Me.dgvDetalleArchivoCargado.Cursor = System.Windows.Forms.Cursors.Default
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDetalleArchivoCargado.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvDetalleArchivoCargado.Location = New System.Drawing.Point(12, 240)
        Me.dgvDetalleArchivoCargado.Margin = New System.Windows.Forms.Padding(2)
        Me.dgvDetalleArchivoCargado.Name = "dgvDetalleArchivoCargado"
        Me.dgvDetalleArchivoCargado.ReadOnly = True
        Me.dgvDetalleArchivoCargado.RowHeadersVisible = False
        Me.dgvDetalleArchivoCargado.RowTemplate.Height = 24
        Me.dgvDetalleArchivoCargado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetalleArchivoCargado.Size = New System.Drawing.Size(985, 244)
        Me.dgvDetalleArchivoCargado.TabIndex = 17
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(814, 166)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(182, 33)
        Me.Button4.TabIndex = 18
        Me.Button4.Text = "Cancelar Archivo"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter3
        '
        Me.VerAcceso2TableAdapter3.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter4
        '
        Me.VerAcceso2TableAdapter4.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter5
        '
        Me.VerAcceso2TableAdapter5.ClearBeforeFill = True
        '
        'Sesion
        '
        Me.Sesion.DataPropertyName = "Sesion"
        Me.Sesion.FillWeight = 6.952122!
        Me.Sesion.HeaderText = "Sesión"
        Me.Sesion.Name = "Sesion"
        Me.Sesion.ReadOnly = True
        Me.Sesion.Visible = False
        '
        'Consecutivo
        '
        Me.Consecutivo.DataPropertyName = "Consecutivo"
        Me.Consecutivo.FillWeight = 78.83848!
        Me.Consecutivo.HeaderText = "Consecutivo"
        Me.Consecutivo.Name = "Consecutivo"
        Me.Consecutivo.ReadOnly = True
        Me.Consecutivo.Visible = False
        '
        'Sucursal
        '
        Me.Sucursal.DataPropertyName = "Sucursal"
        Me.Sucursal.FillWeight = 152.9481!
        Me.Sucursal.HeaderText = "Sucursal"
        Me.Sucursal.Name = "Sucursal"
        Me.Sucursal.ReadOnly = True
        Me.Sucursal.Width = 150
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha"
        Me.Fecha.FillWeight = 303.4594!
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        '
        'Hora
        '
        Me.Hora.DataPropertyName = "Hora"
        Me.Hora.FillWeight = 609.1371!
        Me.Hora.HeaderText = "Hora"
        Me.Hora.Name = "Hora"
        Me.Hora.ReadOnly = True
        '
        'Temporal
        '
        Me.Temporal.DataPropertyName = "Temporal"
        Me.Temporal.FillWeight = 6.952122!
        Me.Temporal.HeaderText = "Temporal"
        Me.Temporal.Name = "Temporal"
        Me.Temporal.ReadOnly = True
        Me.Temporal.Visible = False
        Me.Temporal.Width = 6
        '
        'Monto
        '
        Me.Monto.DataPropertyName = "Monto"
        Me.Monto.FillWeight = 6.952122!
        Me.Monto.HeaderText = "Monto"
        Me.Monto.Name = "Monto"
        Me.Monto.ReadOnly = True
        Me.Monto.Width = 80
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.FillWeight = 6.952122!
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        '
        'IDFactura
        '
        Me.IDFactura.DataPropertyName = "IDFactura"
        Me.IDFactura.FillWeight = 6.952122!
        Me.IDFactura.HeaderText = "ID Factura"
        Me.IDFactura.Name = "IDFactura"
        Me.IDFactura.ReadOnly = True
        Me.IDFactura.Visible = False
        Me.IDFactura.Width = 6
        '
        'Factura
        '
        Me.Factura.DataPropertyName = "Factura"
        Me.Factura.FillWeight = 6.952122!
        Me.Factura.HeaderText = "Factura"
        Me.Factura.Name = "Factura"
        Me.Factura.ReadOnly = True
        '
        'Contrato
        '
        Me.Contrato.DataPropertyName = "Contrato"
        Me.Contrato.FillWeight = 6.952122!
        Me.Contrato.HeaderText = "Contrato"
        Me.Contrato.Name = "Contrato"
        Me.Contrato.ReadOnly = True
        '
        'Nombre
        '
        Me.Nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Nombre.DataPropertyName = "Nombre"
        Me.Nombre.FillWeight = 6.952122!
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        '
        'FrmRecepOxxo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1008, 532)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.dgvDetalleArchivoCargado)
        Me.Controls.Add(Me.dgvCargaArchivos)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(StatusLabel)
        Me.Controls.Add(Me.StatusTextBox)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Clv_SessionLabel)
        Me.Controls.Add(Me.Clv_SessionTextBox)
        Me.Controls.Add(CLIENTELabel)
        Me.Controls.Add(Me.CLIENTETextBox)
        Me.Controls.Add(ReciboLabel)
        Me.Controls.Add(Me.ReciboTextBox)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.MaximizeBox = False
        Me.Name = "FrmRecepOxxo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Recepción del Archivo de Oxxo"
        CType(Me.CONSULTA_Resultado_OxxoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CONSULTATablaClv_Session_OxxoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCargaArchivos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDetalleArchivoCargado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents CONSULTA_Resultado_OxxoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTA_Resultado_OxxoTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.CONSULTA_Resultado_OxxoTableAdapter
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents ReciboTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CLIENTETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONSULTATablaClv_Session_OxxoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONSULTATablaClv_Session_OxxoTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.CONSULTATablaClv_Session_OxxoTableAdapter
    Friend WithEvents Clv_SessionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents dgvCargaArchivos As System.Windows.Forms.DataGridView
    Friend WithEvents dgvDetalleArchivoCargado As System.Windows.Forms.DataGridView
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter3 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter4 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter5 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents Sesion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Consecutivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sucursal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Hora As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Temporal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDFactura As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Factura As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Contrato As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
