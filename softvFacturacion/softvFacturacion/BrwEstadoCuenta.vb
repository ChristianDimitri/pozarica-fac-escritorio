﻿
Imports System.Data.SqlClient
Imports System.Text
Imports System.IO
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class BrwEstadoCuenta

    Dim Op As Integer = 0
    Dim rutaUsuario As String
    Dim cont As Integer = 0
    Dim arregloImagen() As Byte


    Private Sub ConEstadoCuentaStatus()
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ConEstadoCuentaStatus")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            dgEstadoCuentaStatus.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub ConEstadoCuenta(ByVal Fecha As Date)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ConEstadoCuenta '" + Fecha + "'")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            dgEstadoCuenta.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Function DameBanner() As Byte()
        Try
            Dim fs As New FileStream(rutaBanner + "\Banner.jpg", FileMode.Open, FileAccess.Read)
            Dim arreglo(fs.Length) As Byte

            fs.Read(arreglo, 0, arreglo.Length)

            Return arreglo
        Catch ex As Exception

        End Try

    End Function

    Private Sub ImprimirEstadoCuenta(ByVal IdEstadoCuenta As Integer, ByVal Contrato As Long, ByVal Fecha As Date)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC ImprimirEstadoCuenta " + IdEstadoCuenta.ToString())
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dSet As New DataSet
        Dim rDocument As New ReportDocument
        Dim dTable As New DataTable
        Dim dRow As DataRow = dTable.NewRow()


        Try


            dAdapter.Fill(dSet)

            dTable.Columns.Add("Imagen", arregloImagen.GetType())
            dRow("Imagen") = arregloImagen
            dTable.Rows.Add(dRow)

            dSet.Tables.Add(dTable)
            dSet.Tables(0).TableName = "EstadoCuenta"
            dSet.Tables(1).TableName = "DetEstadoCuenta"
            dSet.Tables(2).TableName = "Banner"

            rDocument.Load(RutaReportes + "\ReportEstadoCuentaCorreoSoftv.rpt")
            rDocument.SetDataSource(dSet)
            rDocument.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, rutaUsuario & "\" & Contrato & ".pdf")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            dAdapter.Dispose()
            dSet.Dispose()
            rDocument.Dispose()
            dTable.Dispose()
        End Try

    End Sub

    Private Sub ConEstadoCuentaAImprimir(ByVal Op As Integer, ByVal Fecha As Date, ByVal IdEstadoCuenta As Integer, ByVal imgOxxo As Byte())
        
        Dim dataSet As New DataSet()
        Dim reportDocument As New ReportDocument
        Dim dataTable As New DataTable
        Dim lp As List(Of String) = New List(Of String)
        lp.Add("EstadoCuenta")
        lp.Add("DetEstadoCuenta")
        'lp.Add("ReporteAreaTecnicaQuejas1")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, Fecha.ToString)
        BaseII.CreateMyParameter("@IdEstadoCuenta", SqlDbType.BigInt, IdEstadoCuenta)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@imageOxxo", SqlDbType.Image, imgOxxo)
        dataSet = BaseII.ConsultaDS("ConEstadoCuentaAImprimir", lp)
        Try
            dataSet.Tables(0).TableName = "EstadoCuenta"
            dataSet.Tables(1).TableName = "DetEstadoCuenta"

            'For Each dr As DataRow In dataSet.Tables(0).Rows
            '    dr("CodigoDeBarrasOxxo") = GeneraCodeBar128((dr("oxxo").ToString()))
            'Next

            reportDocument.Load(RutaReportes + "\ReportEstadoCuenta.rpt")
            reportDocument.SetDataSource(dataSet)

            LiTipo = 0
            FrmImprimir.CrystalReportViewer1.ReportSource = reportDocument
            FrmImprimir.Show()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub BrwEstadoCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        ConEstadoCuentaStatus()
        UspGuardaFormularios(Me.Name, Me.Text)
        UspGuardaBotonesFormularioSiste(Me, Me.Name)
        UspDesactivaBotones(Me, Me.Name)
    End Sub

    Private Sub dgEstadoCuentaStatus_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgEstadoCuentaStatus.SelectionChanged
        Try
            If dgEstadoCuentaStatus.Rows.Count = 0 Then Exit Sub
            ConEstadoCuenta(dgEstadoCuentaStatus.SelectedCells(0).Value)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub bnVerTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnVerTodos.Click
        'Muchos
        If dgEstadoCuentaStatus.Rows.Count = 0 Then
            MsgBox("Selecciona un periodo.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        Dim lineaOxxo As String
        Dim imgOxxo As Byte()
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Clv_Session", ParameterDirection.Output, SqlDbType.BigInt)
        BaseII.ProcedimientoOutPut("DameClv_Session")
        LocClv_session = BaseII.dicoPar("@Clv_Session")
        Dim dt As DataTable
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@fecha", SqlDbType.DateTime, dgEstadoCuentaStatus.SelectedCells(0).Value)
        dt = BaseII.ConsultaDT("DameLineaOxxoMuchos")
        Dim size As Integer
        size = dt.Rows.Count
        Dim i As Integer = 0
        While i < size
            lineaOxxo = dt.Rows(i)(0).ToString
            imgOxxo = GeneraCodeBar128Nuevo(lineaOxxo)
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("imgOxxo", SqlDbType.Image, imgOxxo)
            BaseII.CreateMyParameter("@idEstadoCuenta", SqlDbType.BigInt, dt.Rows(i)(1).ToString)
            BaseII.CreateMyParameter("@clv_session", SqlDbType.BigInt, LocClv_session)
            BaseII.Inserta("InsertaImgAux")
            i = i + 1
        End While
        ConEstadoCuentaAImprimirMuchos(0, dgEstadoCuentaStatus.SelectedCells(0).Value, 0, LocClv_session)

    End Sub

    Private Sub bnVer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnVer.Click
        'Individual
        If dgEstadoCuenta.Rows.Count = 0 Then
            MsgBox("Selecciona un estado de cuenta.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        Dim imgOxxo As Byte()
        Dim lineaOxxo As String
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@idEstadoCuenta", SqlDbType.Int, dgEstadoCuenta.SelectedCells(0).Value)
        BaseII.CreateMyParameter("@lineaOxxo", ParameterDirection.Output, SqlDbType.VarChar, 30)
        BaseII.ProcedimientoOutPut("DameLineaOxxo")
        lineaOxxo = BaseII.dicoPar("@lineaOxxo")
        imgOxxo = GeneraCodeBar128Nuevo(lineaOxxo)
        ConEstadoCuentaAImprimir(1, Today, dgEstadoCuenta.SelectedCells(0).Value, imgOxxo)

    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Public Function GeneraCodeBar128Nuevo(ByVal Data As String) As Byte()
        Dim b As New BarcodeLib.Barcode()
        Dim imgBarCode As Byte()
        Dim barcode As New PictureBox()
        Dim W As Integer = Convert.ToInt32(350)
        Dim H As Integer = Convert.ToInt32(100)
        'Dim Data As [String] = "31000016024201101070637005"
        Dim Align As BarcodeLib.AlignmentPositions = BarcodeLib.AlignmentPositions.CENTER
        Align = BarcodeLib.AlignmentPositions.CENTER
        Dim type As BarcodeLib.TYPE = BarcodeLib.TYPE.UNSPECIFIED
        type = BarcodeLib.TYPE.CODE128
        Try
            If type <> BarcodeLib.TYPE.UNSPECIFIED Then
                b.IncludeLabel = True
                b.Alignment = Align
                b.RotateFlipType = (RotateFlipType.RotateNoneFlipNone)
                b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER
                barcode.Image = b.Encode(type, Data, Color.Black, Color.White, W, H)
            End If
            barcode.Width = barcode.Image.Width
            barcode.Height = barcode.Image.Height
            'File.WriteAllBytes(@"C:\XSD\1.jpg", imgbites);                
            Dim imgbites As Byte() = ImageToByte2(barcode.Image)
            imgBarCode = imgbites
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Return imgBarCode
    End Function

    Private Sub ConEstadoCuentaAImprimirMuchos(ByVal Op As Integer, ByVal Fecha As Date, ByVal IdEstadoCuenta As Integer, ByVal clv_session As Integer)
        Dim dataSet As New DataSet()
        Dim reportDocument As New ReportDocument
        Dim dataTable As New DataTable
        Dim lp As List(Of String) = New List(Of String)
        lp.Add("EstadoCuenta")
        lp.Add("DetEstadoCuenta")
        'lp.Add("ReporteAreaTecnicaQuejas1")
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Fecha", SqlDbType.DateTime, Fecha)
        BaseII.CreateMyParameter("@IdEstadoCuenta", SqlDbType.BigInt, IdEstadoCuenta)
        BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
        BaseII.CreateMyParameter("@clv_session", SqlDbType.Int, clv_session)
        dataSet = BaseII.ConsultaDS("ConEstadoCuentaAImprimirMuchos", lp)
        Try
            dataSet.Tables(0).TableName = "EstadoCuenta"
            dataSet.Tables(1).TableName = "DetEstadoCuenta"

            'For Each dr As DataRow In dataSet.Tables(0).Rows
            '    dr("CodigoDeBarrasOxxo") = GeneraCodeBar128((dr("oxxo").ToString()))
            'Next

            reportDocument.Load(RutaReportes + "\ReportEstadoCuenta.rpt")
            reportDocument.SetDataSource(dataSet)

            LiTipo = 0
            FrmImprimir.CrystalReportViewer1.ReportSource = reportDocument
            FrmImprimir.Show()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub
#Region "Perfiles"



    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Function UspDesactivarBotonesCliente(ByVal NombreBoton As String) As Boolean
        Try

            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@NOMBREBOTON", SqlDbType.VarChar, NombreBoton, 250)
            BaseII.CreateMyParameter("@NOMBREFORMULARIO", SqlDbType.VarChar, Me.Name, 250)
            BaseII.CreateMyParameter("@IDUSUARIO", SqlDbType.Int, GloTipoUsuario)

            BotonesDesactivar = BaseII.ConsultaDT("UspDesactivarBotonesClienteFac")

            UspDesactivarBotonesCliente = BotonesDesactivar.Rows(0)(0).ToString

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub UspDesactivaBotones(ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Object In formulario.Controls

            var = Mid(ctl.Name, 1, 3)

            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If

        Next
    End Sub

    Public Sub bwrpanelPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox


        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next


    End Sub

    Public Sub bwrGroupBoxPerfilDesactiva(ByVal GroBx As GroupBox, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In GroBx.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                Panel = New Panel
                Panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                Panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                Split = New SplitContainer
                Split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                Split = Nothing
            End If
        Next


    End Sub
    Public Sub bwrSplitContainerPerfilDesactiva(ByVal panel2 As Panel, ByVal formulario As Form, ByVal NomInterno As String)
        Dim boton As Button
        Dim panel As Panel
        Dim var As String
        Dim split As SplitContainer
        Dim TAB As TabControl
        Dim Menupal As MenuStrip
        Dim data As DataGridView
        Dim BN As BindingNavigator
        Dim GB As GroupBox

        For Each ctl As Control In panel2.Controls
            If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctl
                'boton.Text
                boton.Enabled = UspDesactivarBotonesCliente(boton.Name)
                boton = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) Then
                panel = New Panel
                panel = ctl
                bwrpanelPerfilDesactiva(panel, formulario, NomInterno)
                panel = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.BindingNavigator) Then
                BN = New BindingNavigator
                BN = ctl
                BN.Enabled = UspDesactivarBotonesCliente(BN.Name)
                BN = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GB = New GroupBox
                GB = ctl
                bwrGroupBoxPerfilDesactiva(GB, formulario, NomInterno)
                GB = Nothing
            ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctl
                bwrSplitContainerPerfilDesactiva(split.Panel1, formulario, NomInterno)
                bwrSplitContainerPerfilDesactiva(split.Panel2, formulario, NomInterno)
                split = Nothing
            End If
        Next
    End Sub

    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''Perfiles'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
#End Region
End Class