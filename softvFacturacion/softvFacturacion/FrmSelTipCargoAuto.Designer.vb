<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmSelTipCargoAuto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.MuestraSeleccionTipoCuentaCargosAutotmpconsultaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProcedimientosArnoldo3 = New softvFacturacion.ProcedimientosArnoldo3
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.MuestraSeleccionTipoCuentaCargosAutoconsultaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button3 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button6 = New System.Windows.Forms.Button
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.CMBLabel2 = New System.Windows.Forms.Label
        Me.Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsultaTableAdapter = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsultaTableAdapter
        Me.Muestra_Seleccion_TipoCuenta_CargosAutoconsultaTableAdapter = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.Muestra_Seleccion_TipoCuenta_CargosAutoconsultaTableAdapter
        CType(Me.MuestraSeleccionTipoCuentaCargosAutotmpconsultaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProcedimientosArnoldo3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraSeleccionTipoCuentaCargosAutoconsultaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(440, 293)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 26
        Me.Button2.Text = "&Cancelar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(267, 293)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 25
        Me.Button1.Text = "&Aceptar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.MuestraSeleccionTipoCuentaCargosAutotmpconsultaBindingSource
        Me.ListBox1.DisplayMember = "Tipo_cuenta"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(28, 51)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.ScrollAlwaysVisible = True
        Me.ListBox1.Size = New System.Drawing.Size(196, 212)
        Me.ListBox1.TabIndex = 27
        Me.ListBox1.ValueMember = "Tipo_cuenta"
        '
        'MuestraSeleccionTipoCuentaCargosAutotmpconsultaBindingSource
        '
        Me.MuestraSeleccionTipoCuentaCargosAutotmpconsultaBindingSource.DataMember = "Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsulta"
        Me.MuestraSeleccionTipoCuentaCargosAutotmpconsultaBindingSource.DataSource = Me.ProcedimientosArnoldo3
        '
        'ProcedimientosArnoldo3
        '
        Me.ProcedimientosArnoldo3.DataSetName = "ProcedimientosArnoldo3"
        Me.ProcedimientosArnoldo3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ListBox2
        '
        Me.ListBox2.DataSource = Me.MuestraSeleccionTipoCuentaCargosAutoconsultaBindingSource
        Me.ListBox2.DisplayMember = "Tipo_cuenta"
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(357, 51)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.ScrollAlwaysVisible = True
        Me.ListBox2.Size = New System.Drawing.Size(204, 212)
        Me.ListBox2.TabIndex = 28
        Me.ListBox2.ValueMember = "Tipo_cuenta"
        '
        'MuestraSeleccionTipoCuentaCargosAutoconsultaBindingSource
        '
        Me.MuestraSeleccionTipoCuentaCargosAutoconsultaBindingSource.DataMember = "Muestra_Seleccion_TipoCuenta_CargosAutoconsulta"
        Me.MuestraSeleccionTipoCuentaCargosAutoconsultaBindingSource.DataSource = Me.ProcedimientosArnoldo3
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(243, 51)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(97, 25)
        Me.Button3.TabIndex = 29
        Me.Button3.Text = ">"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(243, 82)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(97, 24)
        Me.Button4.TabIndex = 30
        Me.Button4.Text = ">>"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(243, 174)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(97, 19)
        Me.Button5.TabIndex = 31
        Me.Button5.Text = "<"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(243, 199)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(97, 22)
        Me.Button6.TabIndex = 32
        Me.Button6.Text = "<<"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(25, 27)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(174, 15)
        Me.CMBLabel1.TabIndex = 33
        Me.CMBLabel1.Text = "Tipo De Cargo Automatico"
        '
        'CMBLabel2
        '
        Me.CMBLabel2.AutoSize = True
        Me.CMBLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel2.Location = New System.Drawing.Point(354, 27)
        Me.CMBLabel2.Name = "CMBLabel2"
        Me.CMBLabel2.Size = New System.Drawing.Size(207, 15)
        Me.CMBLabel2.TabIndex = 34
        Me.CMBLabel2.Text = "Tipo De Cargo Automatico Sel.:"
        '
        'Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsultaTableAdapter
        '
        Me.Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsultaTableAdapter.ClearBeforeFill = True
        '
        'Muestra_Seleccion_TipoCuenta_CargosAutoconsultaTableAdapter
        '
        Me.Muestra_Seleccion_TipoCuenta_CargosAutoconsultaTableAdapter.ClearBeforeFill = True
        '
        'FrmSelTipCargoAuto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(590, 342)
        Me.Controls.Add(Me.CMBLabel2)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "FrmSelTipCargoAuto"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmSelTipCargoAuto"
        CType(Me.MuestraSeleccionTipoCuentaCargosAutotmpconsultaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProcedimientosArnoldo3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraSeleccionTipoCuentaCargosAutoconsultaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel2 As System.Windows.Forms.Label
    Friend WithEvents MuestraSeleccionTipoCuentaCargosAutotmpconsultaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProcedimientosArnoldo3 As softvFacturacion.ProcedimientosArnoldo3
    Friend WithEvents MuestraSeleccionTipoCuentaCargosAutoconsultaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsultaTableAdapter As softvFacturacion.ProcedimientosArnoldo3TableAdapters.Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsultaTableAdapter
    Friend WithEvents Muestra_Seleccion_TipoCuenta_CargosAutoconsultaTableAdapter As softvFacturacion.ProcedimientosArnoldo3TableAdapters.Muestra_Seleccion_TipoCuenta_CargosAutoconsultaTableAdapter
End Class
